# Hometuls exercise number 3.
# Sebastian Gonzalez Ramirez
# February 7 2020

import re


def googleRegexp(inptstr):
    """ Function which recieves a string and it's compared with a regexp which determines if it's correctly written or not.
        INPUT : string
        OUTPUT : Boolean
    """
    expression = r"^[gG](o|O|0|\(\)|\[\]|\<\>){2}[gG][lLI][eE3]$"
    hit = re.match(expression, inptstr)
    return False if isinstance(hit, type(None)) else True


# This list was created for testing purposes, can be deleted or replaced.
TEST_INPUTS = [
    'g()()gle',
    'g00gle',
    'g gooogle',
    'G00gIe',
    "g00gle ",
    "g google",
    "GGOOGLE",
    "hey google"]
TEST_OUTPUTS = [True, True, False, True, False, False, False, False]
# iterated over the list testing the function.


def testCases(testinputs, testoutputs):
    for idx, inpt in enumerate(testinputs):
        assert googleRegexp(inpt) == testoutputs[idx]
    print("Tests finished succesfully")


testCases(TEST_INPUTS, TEST_OUTPUTS)
