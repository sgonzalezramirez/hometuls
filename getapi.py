#Hometuls exercise number 1
import requests
import json

APIKEY = "CHANGE-ME"
URL = 'https://34.201.19.114:40003/recordController/getAllRecords'


def getservice(url):
    """ function which recieves a records-url and saves the data into a json file on /static/.
    """
    rqst = requests.get(url, verify=False)
    data = rqst.json()
    with open('/Users/sebastiangonzalezramirez/Documents/Hometuls/static/getallRecords.json', 'w') as outfile:
        json.dump(
            data,
            outfile,
            sort_keys=True,
            indent=4,
            separators=(
                ',',
                ': '),
            ensure_ascii=False)
    return data


def reverseGeocoding(APIKEY, latitude, longitude):
    """ Function which recieves a GoogleMaps -APIKEY- , -latitude- and -longitude- and returns data found by reverse Geocoding.
    """
    gapi = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + \
        str(latitude) + "," + str(longitude) + "&key=" + APIKEY
    request = requests.get(gapi)
    data = request.json()
    # with open('/Users/sebastiangonzalezramirez/Documents/Hometuls/static/adress_lat_long.json', 'w') as outfile:
    #     json.dump(data,outfile,sort_keys=True,indent=4, separators=(',', ': '),ensure_ascii=False)
    return data


def reverseGeoUsers(usersjson):
    """ Function which iterates over the users and finds the address asociated to the lattitude and longitude
        provided by the users retrieved by the getservice function, it saves a json-file on /static/
        where the keys are the user id's and the value is the address found.
    """
    usersdict = dict()
    for user in usersjson:
        try:
            usersdict[user['id']] = {'address': reverseGeocoding(
                APIKEY, user['latitude'], user['longitude'])["results"][0]["formatted_address"]}
        except BaseException:
            print(
                "No latitude or longitude were provided by user with the id",
                user["id"])
    with open('/Users/sebastiangonzalezramirez/Documents/Hometuls/static/USERSDICT.json', 'w') as outfile:
        json.dump(
            usersdict,
            outfile,
            sort_keys=True,
            indent=4,
            separators=(
                ',',
                ': '),
            ensure_ascii=False)


# Functions executed for testing purposes.
usersdata = getservice(URL)

reverseGeoUsers(usersdata)
